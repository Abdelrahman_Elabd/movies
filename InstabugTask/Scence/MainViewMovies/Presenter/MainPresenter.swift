//
//  MainViewDelegate.swift
//  InstabugTask
//
//  Created by Abdelrahman Elabd on 2/22/19.
//  Copyright © 2019 Instabug. All rights reserved.
//

import UIKit

protocol MainDelegate : class {
    func fetchDataSuccess(success : Bool )
    func showError(errorDescription : String)
}

protocol MovieViewCell {
     func appendPosterImage(urlPosterImage : String)
     func appendTitle(title : String)
     func appendOverViewLabelInCell(overViewLabel : String)
     func appendDate(date : String)
}

class MainPresenter : NSObject{
    
    weak var delegate : MainDelegate?
    let moviesURL  = URLS.baseUrl
    var currentModel : [ResponseMovies]? = [ResponseMovies]()
    var currentMovies : [MovieModel]? = [MovieModel]()
    var counterPage = 0
    
    func viewDidLoaded(counterPage : Int){
        getAllMovies(counterPage : counterPage)
    }
    
    
    func getAllMovies(counterPage : Int)  {
        let parameters = ["api_key":"acea91d2bff1c53e6604e4985b6989e2","page": String(counterPage)] as [String : Any]
        
        APIManager.shared.postData(moviesURL, parameters: (parameters as? [String : String])!) { (error, success, data) in
            if data != nil{
                let jsonData = try? JSONSerialization.data(withJSONObject:data as Any)
                let model = try! JSONDecoder().decode(ResponseMovies.self, from: jsonData!)
                for movie in model.results!
                {
                    self.currentMovies?.append(movie)
                }
               
                self.delegate!.fetchDataSuccess(success: true)
            }
            else{
                self.delegate?.showError(errorDescription: error)
            }
        }
    }
    
    func configureCell(currentCell : MainMoviesCollectionViewCell , for index : Int) {
        let currentMoview = currentMovies![index]
        currentCell.appendTitle(title: currentMoview.original_title ?? "" )
        currentCell.appendOverViewLabelInCell(overViewLabel: currentMoview.overview ?? "")
        currentCell.appendDate(date: currentMoview.release_date ?? "")
        currentCell.appendPosterImage(urlPosterImage : currentMoview.poster_path ?? "")
        
    }
    
    func getNumberOfSectionMovies() -> Int {
   
        return ((self.currentMovies?.count)!)
    }
    
}
