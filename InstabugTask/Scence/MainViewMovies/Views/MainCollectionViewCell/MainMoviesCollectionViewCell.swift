//
//  MainMoviesCollectionViewCell.swift
//  InstabugTask
//
//  Created by Abdelrahman Elabd on 2/22/19.
//  Copyright © 2019 Instabug. All rights reserved.
//

import UIKit

class MainMoviesCollectionViewCell: UICollectionViewCell , MovieViewCell{
    
   
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var overViewLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var imgCell: UIImageView!
    
    
    func appendOverViewLabelInCell(overViewLabel: String) {
        self.overViewLabel.text = overViewLabel
    }
    func appendPosterImage(urlPosterImage : String){
        imgCell.contentMode = UIView.ContentMode.scaleAspectFill
        self.imgCell.downloaded(from: urlPosterImage)
    }
    func appendTitle(title: String) {
         self.titleLabel.text = title
    }
    
    func appendDate(date: String) {
        self.dateLabel.text = date
    }
    
}


