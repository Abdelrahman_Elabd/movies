//
//  ViewController.swift
//  InstabugTask
//
//  Created by Abdelrahman Elabd on 2/21/19.
//  Copyright © 2019 Instabug. All rights reserved.
//

import UIKit

class MainViewController: UIViewController, MainDelegate{
    
    @IBOutlet weak var mainMoviesCollectionView: UICollectionView!
    
    var mainPresenter : MainPresenter?
    var numberofRowsInSection = 0
    var counterPage = 1
    var activityIndicator = UIActivityIndicatorView()
    var strLabel = UILabel()
    let effectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
    var counterOFPaging : Int = 0
    
    var mainStoryBoard : UIStoryboard{
        get {
           return UIStoryboard(name: "Main", bundle: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.activityIndicator("Loading.........")
        mainPresenter = MainPresenter()
        mainPresenter?.delegate = self
        mainPresenter!.viewDidLoaded(counterPage : counterPage)
        self.mainMoviesCollectionView.delegate = self
        self.mainMoviesCollectionView.dataSource = self
      
       
    }
    @IBAction func TransferToAddMovieViewController(_ sender: Any) {
        
         let addMovieViewController = mainStoryBoard.instantiateViewController(withIdentifier: "addMovieViewController") as? AddmoviewViewController
        addMovieViewController?.addMoviePresenter?.delegate = self
         self.navigationController?.pushViewController(addMovieViewController!, animated: true)
        
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.mainMoviesCollectionView.collectionViewLayout.invalidateLayout()
    }
    
    
    func showError(errorDescription: String) {
        print(errorDescription)
    }
    
    func fetchDataSuccess(success: Bool) {
   
        if (mainPresenter?.getNumberOfSectionMovies()) == 20{
            self.hideActivity()
            self.mainMoviesCollectionView.reloadData()
        }
        else{
            self.hideActivity()
            let newIndexPaths = ((mainPresenter?.getNumberOfSectionMovies())! - 20...(mainPresenter?.getNumberOfSectionMovies())! - 1).map({ index in
                IndexPath(item: index, section: 0)
            })
            self.mainMoviesCollectionView.performBatchUpdates({
                self.mainMoviesCollectionView.insertItems(at: newIndexPaths)
            })
        }
    }
}

extension MainViewController : AddMovieDelegate{
    func appendNewMovieInCollectionView(posterImage: UIImage, title: String, date: String, overView: String) {
        
    }
}



