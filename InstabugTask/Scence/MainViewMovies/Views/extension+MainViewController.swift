
//  File.swift
//  InstabugTask
//
//  Created by Abdelrahman Elabd on 2/21/19.
//  Copyright © 2019 Instabug. All rights reserved.
//

import UIKit

extension MainViewController : UICollectionViewDelegate , UICollectionViewDataSource , UIScrollViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mainPresenter?.getNumberOfSectionMovies() ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellMovieCollectionView = self.mainMoviesCollectionView.dequeueReusableCell(withReuseIdentifier: "mainmoviescollectionviewcell", for: indexPath) as? MainMoviesCollectionViewCell
        mainPresenter!.configureCell(currentCell : cellMovieCollectionView!,for: indexPath.row)
            return cellMovieCollectionView!
        }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
      
        return CGSize(width: collectionView.bounds.size.width, height: CGFloat(collectionView.bounds.size.height/3))
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if indexPath.row == (mainPresenter?.getNumberOfSectionMovies())! - 1 {
           self.showActivityIndicatorWithoutView()
            counterPage += 1
            mainPresenter!.viewDidLoaded(counterPage : counterPage)
            
        }
            
        }
    
    func activityIndicator(_ title: String) {
        
        strLabel.removeFromSuperview()
        activityIndicator.removeFromSuperview()
        effectView.removeFromSuperview()
        
        strLabel = UILabel(frame: CGRect(x: 50, y: 0, width: 160, height: 46))
        strLabel.text = title
        strLabel.font = .systemFont(ofSize: 14, weight: .medium)
        strLabel.textColor = UIColor(white: 0.9, alpha: 0.7)
        
        effectView.frame = CGRect(x: view.frame.midX - strLabel.frame.width/2, y: view.frame.midY - strLabel.frame.height/2 , width: 160, height: 46)
        effectView.layer.cornerRadius = 15
        effectView.layer.masksToBounds = true
        
        activityIndicator = UIActivityIndicatorView(style: .white)
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 46, height: 46)
        activityIndicator.startAnimating()
        
        effectView.contentView.addSubview(activityIndicator)
        effectView.contentView.addSubview(strLabel)
        mainMoviesCollectionView.addSubview(effectView)
    }
    
    
    func hideActivity()  {
            self.activityIndicator.stopAnimating()
            self.effectView.isHidden = true
}
    func showActivityIndicatorWithoutView()  {
        activityIndicator.frame = CGRect(x: self.mainMoviesCollectionView.frame.midX, y: self.view.frame.maxY, width: 30.0, height: 30.0)
       self.mainMoviesCollectionView.addSubview(activityIndicator)
        activityIndicator.color = UIColor.black
        activityIndicator.startAnimating()
     
    }
    
    
    
    
}
