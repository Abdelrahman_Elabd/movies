//
//  ResponseMoviesModel.swift
//  InstabugTask
//
//  Created by Abdelrahman Elabd on 2/21/19.
//  Copyright © 2019 Instabug. All rights reserved.
//

import Foundation
import UIKit

struct ResponseMovies : Decodable{
    var page: Int?
    var total_results : Int?
    var total_pages : Int?
    var results : [MovieModel]? = [MovieModel]()
}

struct MovieModel :  Decodable {
    
    var vote_count: Int?
     var id : Int?
    var video : Bool?
    var vote_average: Double?
    var title : String?
    var popularity : Double?
    var poster_path: String?
    var original_language : String?
    var original_title : String?
    var genre_ids: [Int]? = [Int]()
    var backdrop_path : String?
    var adult : Bool?
    var overview : String?
    var release_date : String?
    
    
}

class AddMovie {
    var posterImage : UIImage?
    var title : String? 
    var details : String?
}


