//
//  AddmoviewViewController.swift
//  InstabugTask
//
//  Created by Abdelrahman Elabd on 2/22/19.
//  Copyright © 2019 Instabug. All rights reserved.
//

import UIKit



class AddmoviewViewController: UIViewController , UITextViewDelegate , UINavigationControllerDelegate {

    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var overViewTextView : UITextView!
    @IBOutlet weak var posterImage: UIImageView!
    
    var checkedImage : Bool = false
    let datePicker = UIDatePicker()
    var addMoviePresenter : AddMoviePresenter?

    var imagePicker: UIImagePickerController!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        overViewTextView.delegate = self
        overViewTextView.text = "Add Over View"
        overViewTextView.textColor = UIColor.lightGray
        dateTextField.inputView = datePicker
        let toolBar = UIToolbar().ToolbarPiker(mySelect: #selector(AddmoviewViewController.dismissPicker))
           dateTextField.inputAccessoryView = toolBar
        addMoviePresenter = AddMoviePresenter()
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    @IBAction func uploadPhotoFromGallary(_ sender: UITapGestureRecognizer) {
        selectImageFrom()
    }
    @objc func dismissPicker() {
        self.dateTextField.text = Helper.convertDateFormater(datePicker.date.description)
        view.endEditing(true)
    }
    func selectImageFrom(){
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true, completion: nil)
    }
    @objc func dateChanged(_ sender: UIDatePicker) {
        let components = Calendar.current.dateComponents([.year, .month, .day], from: sender.date)
        if let day = components.day, let month = components.month, let year = components.year {
        self.dateTextField.text = "\(day) \(month) \(year)"
        }
    }
    
    


    //MARK: - Add image to Library
func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            showAlertWith(title: "Save error", message: error.localizedDescription)
        } else {
            showAlertWith(title: "Saved!", message: "Your image has been saved to your photos.")
        }
    }
    
    func showAlertWith(title: String, message: String){
        let ac = UIAlertController(title: title, message: message, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
    }
    
    
    
    @IBAction func addMovie(_ sender: Any) {
        
        if !(checkedImage){
            self.showAlertWith(title: "Alert", message: "Please Choose Poster Image")
            return
        }
        
        if titleTextField.text?.trimmingCharacters(in: .whitespaces) == ""{
            self.showAlertWith(title: "Alert", message: "PLease enter Title")
            return
        }
        
        
        
        
        
    }
    
    
}

extension AddmoviewViewController: UIImagePickerControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        
        imagePicker.dismiss(animated: true, completion: nil)
        guard let selectedImage = info[.originalImage] as? UIImage else {
            print("Image not found!")
            return
        }
        checkedImage = true
        posterImage.image = selectedImage
        
    }
}
