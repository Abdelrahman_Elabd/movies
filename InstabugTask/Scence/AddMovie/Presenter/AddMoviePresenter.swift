//
//  AddMoviePresenter.swift
//  InstabugTask
//
//  Created by Abdelrahman Elabd on 2/24/19.
//  Copyright © 2019 Instabug. All rights reserved.
//

import UIKit



protocol AddMovieDelegate : class {
    func appendNewMovieInCollectionView(posterImage :UIImage ,title : String , date : String , overView : String)
}

class AddMoviePresenter {
    
    weak var delegate : AddMovieDelegate?
    func addNewMovie(posterImage : UIImage , title: String ,overView: String , date : String) {
        delegate?.appendNewMovieInCollectionView(posterImage: posterImage, title: title, date: date, overView: overView)
    
    }
    

}
