//
//  Helper.swift
//  InstabugTask
//
//  Created by Abdelrahman Elabd on 2/24/19.
//  Copyright © 2019 Instabug. All rights reserved.
//

import UIKit

class Helper {

    static func convertDateFormater(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss z"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return  dateFormatter.string(from: date!)
        
    }
    
    
}
