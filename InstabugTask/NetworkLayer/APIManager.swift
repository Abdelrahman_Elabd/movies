//
//  APIManager.swift
//  InstabugTask
//
//  Created by Abdelrahman Elabd on 2/21/19.
//  Copyright © 2019 Instabug. All rights reserved.
//

import UIKit




typealias data = (_ error: String ,_ success: String,_ data: Any?) -> Void


final class APIManager {
    
    static let shared = APIManager()
    
    private init() {
        
    }
    public func postData(_ urlString: String,parameters: [String: String],
                                            completion: @escaping data) {
        
        var components = URLComponents(string: urlString)!
        components.queryItems = parameters.map { (key, value) in
            URLQueryItem(name: key, value: value)
        }
          components.percentEncodedQuery = components.percentEncodedQuery?.replacingOccurrences(of: "+", with: "%2B")
        let request = URLRequest(url: components.url!)
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let data = data{
                 DispatchQueue.main.async { // Correct
            if error != nil{
                print(error.debugDescription)
                completion(error!.localizedDescription , "failed" , nil )
            }
            else {
                let responseObject = (try? JSONSerialization.jsonObject(with: data)) as? [String: Any]
                    completion("nil" , "Success" ,responseObject)
            }
                }
            }
            }.resume()
}
}
